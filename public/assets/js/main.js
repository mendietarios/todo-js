(function () {
	const module = {
		list: [],
		editing: false,
		init: function () {
			this.cacheDom();
			this.bindingEvents();
			this.showTask();
		},

		// Captura de los elementos ID
		cacheDom: function () {
            this.input = document.getElementById('inputTask');
            this.button = document.getElementById('addButton');
			this.listTask = document.getElementById('tasks');
		},

		//Captura de los eventos
		bindingEvents: function () {
			this.button.addEventListener('click', this.addTask.bind(this));
		},

		// Creacion del los elementos de la lista
		createTaskElement: function (taskValue) {
			const itemTask = document.createElement('li');
			const span = document.createElement('span');
            span.classList.add('text');
            span.innerText = taskValue.name;

			const checkbox = document.createElement('input');
            checkbox.classList.add('task-checkbox');
            checkbox.setAttribute('type', 'checkbox');
			checkbox.setAttribute('id', taskValue.id);
            checkbox.checked = taskValue.checked;
			checkbox.addEventListener('click', this.markAsDone.bind(this));
			itemTask.appendChild(checkbox);
			

			const editButton = document.createElement('button');
            const editIcon = document.createElement('i');
            editButton.setAttribute('type', 'button');
			editButton.classList.add('button', 'icon', 'edit');
            editIcon.classList.add('fa-solid', 'fa-pen-to-square', 'fa-fade');
            editButton.addEventListener('click', this.editTask.bind(this));
            editButton.appendChild(editIcon);
            editButton.setAttribute('id', taskValue.id);
            editIcon.setAttribute('id', taskValue.id);

			const deleteButton = document.createElement('button');
			const deleteIcon = document.createElement('i');
            deleteButton.classList.add('button', 'icon', 'delete');
			deleteIcon.classList.add('fa-solid', 'fa-trash-can', 'fa-fade');
			deleteButton.addEventListener('click', this.deleteTask.bind(this));
            deleteButton.appendChild(deleteIcon);
            deleteButton.setAttribute('id', taskValue.id);
            deleteIcon.setAttribute('id', taskValue.id);

            itemTask.classList.add('task');
			itemTask.appendChild(span);

            const buttonContainer = document.createElement('div');
            buttonContainer.classList.add('buttonContainer');
            buttonContainer.appendChild(deleteButton);
			buttonContainer.appendChild(editButton);
            
            itemTask.appendChild(buttonContainer);
			itemTask.setAttribute('id', taskValue.id);
			this.listTask.appendChild(itemTask);
		},

		// Anhadir los elementos de la lista
        addTask: function (e) {
			e.preventDefault();
			if (!this.input.value) {
				alert("EMPTY TASK!!!");
				return;
			}
			if (this.editing) {
				this.button.innerHTML = '<i class="fa-solid fa-calendar-plus"></i>';
				const newList = [...this.list];
				const itemIndex = this.list.findIndex((element) => element.id === this.idTask);
				const item = this.list.find((element) => element.id === this.idTask);

				item.name = this.input.value;
				newList[itemIndex] = item;

				this.list = newList;
				this.editing = false;
				this.idTask = null;
			} else {
				const task = {
					id: new Date(),
					name: this.input.value,
					checked: false
				}
				this.createTaskElement(task);
				this.list.push( task );
			}

			this.setStorage();
			this.input.value = '';
			this.showTask();
		},

		// Mostrar los elementos de la lista
        showTask: function () {
			this.listTask.innerHTML = '';
			this.list = this.getStorage();
			this.list.forEach((element) => {
				this.createTaskElement(element);
			});
		},

		// Setear elementos en el Local Storage
		setStorage: function () {
			localStorage.setItem('todoItems', JSON.stringify(this.list));
		},

		getStorage: function () {
			const itemTask = JSON.parse(localStorage.getItem('todoItems'));
			
			if (!itemTask) {
				return [];
			}
			return itemTask;
		},

		
		markAsDone: function (e) {
			const idTask = e.target.id;
			const newList = [...this.list];
			const itemIndex = this.list.findIndex((element) => element.id === idTask);
			const item = this.list.find((element) => element.id === idTask);

			item.checked = !item.checked;
			newList[itemIndex] = item;
			
			this.list = newList;
			this.setStorage();
			this.showTask();
		  },

		// Eliminando los elementos de la lista
		deleteTask: function (e) {
			const idTask = e.target.id;
			const newList = [...this.list];
			const filteredList = newList.filter((element) => element.id !== idTask);

			this.list = filteredList;
			this.setStorage();
			this.showTask();
		},

		// Editando los elementos de la lista
		editTask: function (e) {
			const idTask = e.target.id;
			const item = this.list.find((element) => element.id === idTask)

			this.input.value = item.name;
			this.editing = true;
			this.idTask = idTask;
			this.button.innerHTML = '<i class="fa-solid fa-floppy-disk fa-bounce"></i>';
		},
	};
	module.init();
})();
